# Hubot

Hubot is a chat bot, modelled after GitHub's Campfire bot, hubot.

At Fixmo we use Hipchat for internal communication. Hubot sits in our chat at all hours and provides us some company and utilities. We use multiple hubots to accomplish our goals (i.e., instance of hubot exists on multiple machines) all communicating through the same Hipchat account.

For the time being his functions are quite simple (i.e., *pug bombing*, *mustaching*, etc…). We plan to increase his functionality around our daily operations. For example, we hope to achieve functionality which allows us to kick off automatic installers, tests and deployments for our environments.

So the purpose of this repo is to build up our hubot to ultimately reach these goals. While we are at it, why not throw some fun things in there too?

## Requirements

Our Hubot installation requires some software requirements as well as environment variables set up on the machines.

### Software

For Hubot to run we require [Node.js 0.10.18+](http://nodejs.org/), this only allows Hubot to run in shell mode. For our purposes we want Hubot to run in Hipchat. This requires additional dependancies namely [Python 2.7.x](http://www.python.org/) and a C++ compiler.

On a Linux/Unix machine you can simply get away by having Node.js and Python installed. For a Windows environment (which is our typical deployment environment) you required two additional dependencies for the C++ compiler. [Microsoft Visual Studio C++ 2010 Express Edition](http://go.microsoft.com/?linkid=9709949) and the  [Microsoft Windows SDK for Windows 7 and .NET Framework 4](http://www.microsoft.com/en-us/download/details.aspx?id=8279) allow Windows environments to install the Hipchat adaptor required for Hubot.

To avoid the additional Microsoft dependancies we will pre-compile Hubot on a network drive (and only allow one machine to have these dependencies for additional recompiling). This way on Windows environments we can still run Hubot with Hipchat using only Node.js and Python. We keep Python as Hubot will call Python scripts to interact with the underlying machine.

----

**Requirements for all machines:**

* **[Node.js 0.10.18+](http://nodejs.org/)**
* **[Python 2.7.x](http://www.python.org/)**

**Additional requirements for the compiling machine:**

* **[Microsoft Visual Studio C++ 2010 Express Edition](http://go.microsoft.com/?linkid=9709949)**
* **[Microsoft Windows SDK for Windows 7 and .NET Framework 4](http://www.microsoft.com/en-us/download/details.aspx?id=8279)**

----


### Environment Variables

We use environment variables as a means to setup the different instances of hubot on the machines.

**HUBOT_HIPCHAT_JID**: The Hipchat account XMPP user information for hubot to use. For the common account we currently use `30983_204481@chat.hipchat.com`.

**HUBOT_HIPCHAT_PASSWORD**: The Hipchat account XMPP password information for hubot to use. For the common account we currently use `FIXmo123`.

**HUBOT_NAME**: Each hubot instance requires an instance name (this allows us to distinguish between the different instances using the same Hipchat account). They should follow the naming convention: *<machine>_<purpose>* (i.e., *dev02_EMP*, *dev01_TestServer*, etc…). There must also exist one hubot instance using the *main* instance name, this is the hubot which response to *common* commands.

**MASTER_HUBOT_ROOT_PATH**: The path to the network drive holding hubot's pre-compiled source.

**HUBOT_ROOT_PATH**: The path to the local copy of hubot the current machine.

## Install

To simplify installation procedures we keep a git repository on a shared network drive (`filer\dev\hubot`).

----

The following steps copy the pre-compiled source from the network driver to a local location on the machine hubot is being installed on:

1. Ensure the necessary environment variables are set
2. Navigate to the network drive's `hubot` directory in a terminal
3. Execute `python tasks/hubot.py start`

----

At this point there will be a local copy of the pre-compiled hubot source on the machine. The final command also starts up hubot using the location of the local installation of hubot. Hubot is now ready to receive commands on Hipchat.

## Scripts

Hubot uses *coffeescript* to handle everything. It is listening all the time, and it will attempt to respond to any command it hears. To illustrate this we will look at the trimmed down [ping.coffee](src/scripts/ping.coffee) file:

    # Description:
    #   Utility commands surrounding Hubot uptime.
    #
    # Commands:
    #   hubot ping - Reply with pong

    module.exports = (robot) ->
      robot.respond /PING$/i, (msg) ->
        if robot.instance_name == "main"
          msg.send "PONG"

In this example we can see some simple documentation at the top of the file. This is necessary as it is used in the *help* command. In the actual meat of this file we are mainly concerned with the last three lines. The `robot.respond /PING$/i, (msg) ->` part specifies that hubot is listening for any command which matches the regular expression (in this case listening for a *ping*). Then this `msg` is passed down for further use, which only occurs if a match was made.
The next line we perform a simple test for the `instance_name`, this is done because we can have multiple named hubots running from one Hipchat account. For the general commands we let only the *main* hubot instance handle the action. This part here is the only *major* change from the default hubot source. The last line is what hubot performs given the `msg`, in this case it simply responds with a *pong*.

As you can see it is rather simple to add new scripts for our hubot. **The only thing to keep in mind is that you need to specify which instance the command is meant for**. If this part is left out than all hubots will respond at the same time!

To test out any new scripts you are creating simply start hubot in shell mode: `python tasks\hubot.py start --debug`.

# Build Deployment

It order to use Hubot to install and uninstall build from ftp, Hubot must be running on 3 separate machines.  We have been using EMP2008, SQLServer and TestServer.  EMP2008 is used to install the Fixmo EMP Core and/or DMZ.  Hubot will drop the database on SQLServer at the beginning of every installation.  TestServer is used to run the selenium UI Tests.

## Machine Specific Environment Variables

### EMP2008 Environment Variables

**INSTALLER_PATH**: The path to the Installer Test Tool directory

**RELAY_BUNDLE_PATH**: The path to the relay configuration bundle POT file

**FTP_PARENT**: The path to the directory containing all builds of the master branch on FTP

**FTP_HOST**: FTP server url

**FTP_PORT**: FTP server port (we are using sftp)

**FTP_CREDENTIALS**: Location of the file containing the username and password of the FTP user (requires ftp-user and ftp-password variables)

### SQLServer Environment Variables

**SQL_USER**: SQL server username

**SQL_PASS**: SQL server password

**SQL_DB**: Database to connect to on SQLServer

### TestServer Environment Variables

**UI_TEST_PATH**: Directory containing all UI Test files

## Machine Specific Dependencies

EMP2008 and TestServer also require all dependencies to run their respective tools.  EMP2008 will also require any [Installer Test Tool](https://fixmo-eng.jira.com/wiki/display/FP/Automated+Installer+Install+and+Tests) dependencies and TestServer will require any [UI Test](https://fixmo-eng.jira.com/wiki/display/QA/UI+Automation+Documentation) dependencies.

## License

Copyright (c) 2011-2013 GitHub, Inc. See the LICENSE file for license rights and
limitations (MIT).
