# Description:
#   None
#
# Dependencies:
#   None
#
# Configuration:
#   FILE_BRAIN_PATH
#
# Commands:
#   None
#
# Author:
#   dustyburwell

# NOTE: File name has been prefixed with 'z-' to ensure that it is the last script loaded
#       this allows the robot.brain.on 'load' to trigger in other scripts otherwise
#       those scripts might not have been loaded yet, this missing the 'load' emit

fs   = require 'fs'
path = require 'path'

module.exports = (robot) ->
  brainPath = process.env.HUBOT_PATH or '/var/hubot'
  brainPath = path.join brainPath + '/brains/', robot.instance_name + '-brain-dump.json'

  try
    data = fs.readFileSync brainPath, 'utf-8'
    if data
      robot.brain.mergeData JSON.parse(data)
  catch error
      console.log('Unable to read file', error) unless error.code is 'ENOENT'

  robot.brain.on 'save', (data) ->
    fs.writeFileSync brainPath, JSON.stringify(data), 'utf-8'
