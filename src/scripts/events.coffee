# Description:
#   Event system related utilities
#
# Commands:
#   hubot fake event <event> - Triggers the <event> event for debugging reasons
#
# Events:
#   debug - {user: <user object to send message to>}

util = require 'util'

module.exports = (robot) ->

  robot.respond /FAKE EVENT (.*)/i, (msg) ->
    if robot.instance_name == "master"
      msg.send "fake event '#{msg.match[1]}' triggered"
      robot.emit msg.match[1], {user: msg.message.user}

  robot.on 'debug', (event) ->
    if robot.instance_name == "master"
      robot.send event.user, util.inspect event
