# Description:
#   Shows TTC alerts
#
# Dependencies:
#   "htmlparser": "1.7.6"
#   "soupselect": "0.2.0"
#
# Configuration:
#   None
#
# Commands:
#   hubot ttc alerts - Show the current TTC alerts
#
# Author:
#   Kevin Jalbert

Select     = require("soupselect").select
HTMLParser = require "htmlparser"

module.exports = (robot) ->

  robot.respond /ttc alerts/i, (msg) ->
    if robot.instance_name == "master"
      ttcAlerts robot, "http://www.ttc.ca/Service_Advisories/all_service_alerts.jsp", (messages, times) ->
        response = []
        for i in [0..messages.length-1] by 1
          response.push(messages[i] + "\n" + times[i])
        msg.send response.join('\n\n')

ttcAlerts = (robot, url, cb) ->
  robot.http(url)
    .header('User-Agent', 'Hubot Script')
    .get() (err, res, body) ->
      return cb "Sorry, the tubes are broken." if err

      messages = []
      times = []

      # Parse alerts
      alerts = parseHTML(body, "div.advisory-wrap")

      contents = Select alerts, "p.veh-replace"
      for content in contents
        messages.push content.children[0].raw

      timestamps = Select alerts, "p.alert-updated"
      for timestamp in timestamps
        times.push timestamp.children[0].raw

      cb messages, times

parseHTML = (html, selector) ->
  handler = new HTMLParser.DefaultHandler((() ->),
    ignoreWhitespace: true
  )
  parser  = new HTMLParser.Parser handler
  parser.parseComplete html
  Select handler.dom, selector
