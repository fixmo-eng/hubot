# Description:
#   Starts the automated Installer tests
#
# Dependencies:
#   child_process
#
# Configuration:
#   None
#
# Commands:
#  hubot (.*) install (core|dmz|all) of build (.*) - Install the specified build on the hubot instance

{spawn, exec} = require 'child_process'

module.exports = (robot) ->
  robot.router.post "/hubot/build_senv", (req, res) ->
    res.end "Connection closed"
    if robot.instance_name != "master"
      cache = robot.brain.getStatus()

      if !cache['locked']
        robot.brain.set("installer_running", true)
        robot.brain.setStatus({message: "Downloading Installers", start_time: new Date().getTime(), locked:true})
        exec "python tasks/installer.py setup_files", (err, stdout, stderr) ->
          robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Downloading Installers on " + robot.instance_name
          if stderr
            robot.messageRoom "30983_hubot@conf.hipchat.com", "Error setting up files on " + robot.instance_name
            robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error setting up files on " + robot.instance_name
            robot.messageRoom "30983_hubot@conf.hipchat.com", stderr

          http = require 'http'
          data = "{}"
          options = {
            host:'sqlserver.fixmo.local',
            port:8080
            path:'/hubot/sqlserver/drop',
            method:'POST',
            headers:{
              'Content-Type':'application/json',
              'Content-Length':Buffer.byteLength(data,'utf8')
            }
          }

          sql_req = http.request options, (response) ->
            robot.brain.setStatus({message: "Installing Fixmo EMP", start_time: new Date().getTime(), locked:true})
            exec "python tasks/installer.py run all", (err, stdout, stderr) ->
              robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Installing on " + robot.instance_name
              robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
              if stderr
                robot.messageRoom "30983_hubot@conf.hipchat.com", "Error Installing on " + robot.instance_name
                robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error Installing on " + robot.instance_name
                robot.messageRoom "30983_hubot@conf.hipchat.com", stderr

              robot.brain.setStatus({message: "Testing Installation of Fixmo EMP", start_time: new Date().getTime(), locked:true})
              exec "python tasks/installer.py test all", (err, stdout, stderr) ->
                robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Testing Installation  on " + robot.instance_name
                robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
                if stderr
                  robot.messageRoom "30983_hubot@conf.hipchat.com", "Error Testing Installation on " + robot.instance_name
                  robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error Testing Installation on " + robot.instance_name
                  robot.messageRoom "30983_hubot@conf.hipchat.com", stderr

                http = require 'http'
                data = "{}"
                options = {
                  host:'10.1.1.71',
                  port:8080
                  path:'/hubot/master/ui_tests_long_timeout',
                  method:'POST',
                  headers:{
                    'Content-Type':'application/json',
                    'Content-Length':Buffer.byteLength(data,'utf8')
                  }
                }

                ui_req = http.request options, (ui_res) ->
                  robot.brain.setStatus({message: "Uninstalling Fixmo EMP", start_time: new Date().getTime(), locked:true})
                  exec "python tasks/installer.py uninstall all", (err, stdout, stderr) ->
                    robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Uninstalling on " + robot.instance_name
                    robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
                    if stderr
                      robot.messageRoom "30983_hubot@conf.hipchat.com", "Error uninstalling on " + robot.instance_name
                      robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error uninstalling on " + robot.instance_name
                      robot.messageRoom "30983_hubot@conf.hipchat.com", stderr
                    robot.brain.set("installer_running", false)
                    robot.brain.setDefaultStatus()

                ui_req.on 'socket', (socket) ->
                  socket.setTimeout(3600)

                ui_req.on 'error', (e) ->
                  robot.messageRoom "30983_hubot@conf.hipchat.com", "Error occurred contacting TestServer"

                ui_req.write data
                ui_req.end

          sql_req.on 'error', (e) ->
            robot.messageRoom "30983_hubot@conf.hipchat.com", "Error occurred contacting SQLServer"

          sql_req.write data
          sql_req.end


      else
        if !robot.brain.get("new") || !robot.brain.get("installer_running")
          robot.brain.set("installer_running", true)
          robot.brain.set("new",true)
          while robot.brain.get("new")
            setTimeout (->
              if !cache['locked']
                robot.brain.set("new":false)
                robot.brain.setStatus({message: "Downloading Installers", start_time: new Date().getTime(), locked:true})
                exec "python tasks/installer.py setup_files", (err, stdout, stderr) ->
                  robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Downloading Installers on " + robot.instance_name
                  if stderr
                    robot.messageRoom "30983_hubot@conf.hipchat.com", "Error setting up files on " + robot.instance_name
                    robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error setting up files on " + robot.instance_name
                    robot.messageRoom "30983_hubot@conf.hipchat.com", stderr

                  http = require 'http'
                  data = "{}"
                  options = {
                    host:'sqlserver.fixmo.local',
                    port:8080
                    path:'/hubot/sqlserver/drop',
                    method:'POST',
                    headers:{
                      'Content-Type':'application/json',
                      'Content-Length':Buffer.byteLength(data,'utf8')
                    }
                  }

                  sql_req = http.request options, (response) ->
                    robot.brain.setStatus({message: "Installing Fixmo EMP", start_time: new Date().getTime(), locked:true})
                    exec "python tasks/installer.py run all", (err, stdout, stderr) ->
                      robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Installing on " + robot.instance_name
                      robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
                      if stderr
                        robot.messageRoom "30983_hubot@conf.hipchat.com", "Error Installing on " + robot.instance_name
                        robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error Installing on " + robot.instance_name
                        robot.messageRoom "30983_hubot@conf.hipchat.com", stderr

                      robot.brain.setStatus({message: "Testing Installation of Fixmo EMP", start_time: new Date().getTime(), locked:true})
                      exec "python tasks/installer.py test all", (err, stdout, stderr) ->
                        robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Testing Installation  on " + robot.instance_name
                        robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
                        if stderr
                          robot.messageRoom "30983_hubot@conf.hipchat.com", "Error Testing Installation on " + robot.instance_name
                          robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error Testing Installation on " + robot.instance_name
                          robot.messageRoom "30983_hubot@conf.hipchat.com", stderr

                        http = require 'http'
                        data = "{}"
                        options = {
                          host:'10.1.1.71',
                          port:8080
                          path:'/hubot/master/ui_tests',
                          method:'POST',
                          headers:{
                            'Content-Type':'application/json',
                            'Content-Length':Buffer.byteLength(data,'utf8')
                          }
                        }

                        ui_req = http.request options, (ui_res) ->
                          robot.brain.setStatus({message: "Uninstalling Fixmo EMP", start_time: new Date().getTime(), locked:true})
                          exec "python tasks/installer.py uninstall all", (err, stdout, stderr) ->
                            robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Uninstalling on " + robot.instance_name
                            robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
                            if stderr
                              robot.messageRoom "30983_hubot@conf.hipchat.com", "Error uninstalling on " + robot.instance_name
                              robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error uninstalling on " + robot.instance_name
                              robot.messageRoom "30983_hubot@conf.hipchat.com", stderr
                            robot.brain.set("installer_running", false)
                            robot.brain.setDefaultStatus()

                        ui_req.on 'error', (e) ->
                          robot.messageRoom "30983_hubot@conf.hipchat.com", "Error occurred contacting TestServer"

                        ui_req.write data
                        ui_req.end

                    robot.messageRoom "30983_hubot@conf.hipchat.com", "SQLServer: " + JSON.stringify(res)

                  sql_req.on 'error', (e) ->
                    robot.messageRoom "30983_hubot@conf.hipchat.com", "Error occurred contacting SQLServer"

                  sql_req.write data
                  sql_req.end

            ), 5000

  robot.respond /(.*) install (core|dmz|all) of build (.*) from url/i, (msg) ->
    if robot.instance_name == msg.match[1]
      cache = robot.brain.getStatus()

      if cache['locked']
        robot.messageRoom "30983_hubot@conf.hipchat.com", "Installer already running on " + robot.instance_name
      else
        robot.messageRoom "30983_hubot@conf.hipchat.com", "Starting Installer on " + robot.instance_name
        robot.brain.setStatus({message: "working on Installer", start_time: new Date().getTime(), locked: true})

        arg = msg.match[2]
        build = msg.match[3]

        exec "python tasks/installer.py setup_files --build=" + build, (err, stdout, stderr) ->
          robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished downloading files on " + robot.instance_name
          robot.messageRoom "30983_hubot@conf.hipchat.com", stdout

          http = require 'http'
          data = "{}"
          options = {
            host:'sqlserver.fixmo.local',
            port:8080
            path:'/hubot/sqlserver/drop',
            method:'POST',
            headers:{
              'Content-Type':'application/json',
              'Content-Length':Buffer.byteLength(data,'utf8')
            }
          }

          sql_req = http.request options, (response) ->
            robot.brain.setStatus({message: "running Installer", start_time: new Date().getTime(), locked: true})
            exec "python tasks/installer.py run " + arg, (err, stdout, stderr) ->
              robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Installing " + arg + " on " + robot.instance_name
              robot.messageRoom "30983_hubot@conf.hipchat.com", stdout

              exec "python tasks/installer.py test " + arg, (err, stdout, stderr) ->
                robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Testing " + arg + " on " + robot.instance_name
                robot.messageRoom "30983_hubot@conf.hipchat.com", stdout

                http = require 'http'
                data = "{}"
                options = {
                  host:'10.1.1.71',
                  port:8080
                  path:'/hubot/master/ui_tests',
                  method:'POST',
                  headers:{
                    'Content-Type':'application/json',
                    'Content-Length':Buffer.byteLength(data,'utf8')
                  }
                }

                ui_req = http.request options, (ui_res) ->
                  msg.send "Finished running installer on " + robot.instance_name
                  cache = robot.brain.setDefaultStatus()

                ui_req.on 'error', (e) ->
                  robot.messageRoom "30983_hubot@conf.hipchat.com", "Error occurred contacting TestServer"
                  robot.brain.setDefaultStatus()

                ui_req.write data
                ui_req.end

          sql_req.on 'error', (e) ->
            robot.messageRoom "30983_hubot@conf.hipchat.com", "Error occurred contacting SQLServer"
            robot.brain.setDefaultStatus()

          sql_req.write data
          sql_req.end

  robot.respond /(.*) install (core|dmz|all) of build (.*)/i, (msg) ->
    if robot.instance_name == msg.match[1]
      cache = robot.brain.getStatus()

      if cache['locked']
        robot.messageRoom "30983_hubot@conf.hipchat.com", "Installer already running on " + robot.instance_name
      else
        robot.messageRoom "30983_hubot@conf.hipchat.com", "Starting Installer on " + robot.instance_name
        robot.brain.setStatus({message: "working on Installer", start_time: new Date().getTime(), locked: true})

        arg = msg.match[2]
        build = msg.match[3]

        exec "python tasks/installer.py setup_files --build=" + build, (err, stderr, stdout) ->
          robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished setting up files on " + robot.instance_name
          robot.messageRoom "30983_hubot@conf.hipchat.com", stdout

          http = require 'http'
          data = "{}"
          options = {
            host:'sqlserver.fixmo.local',
            port:8080
            path:'/hubot/sqlserver/drop',
            method:'POST',
            headers:{
              'Content-Type':'application/json',
              'Content-Length':Buffer.byteLength(data,'utf8')
            }
          }

          sql_req = http.request options, (response) ->
            robot.brain.setStatus({message: "running Installer", start_time: new Date().getTime(), locked: true})
            exec "python tasks/installer.py run " + arg, (err, stdout, stderr) ->
              robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Installing " + arg + " on " + robot.instance_name
              robot.messageRoom "30983_hubot@conf.hipchat.com", stdout

              exec "python tasks/installer.py test " + arg, (err, stdout, stderr) ->
                robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished Testing " + arg + " on " + robot.instance_name
                robot.messageRoom "30983_hubot@conf.hipchat.com", stdout

                http = require 'http'
                data = "{}"
                options = {
                  host:'10.1.1.71',
                  port:8080
                  path:'/hubot/master/ui_tests',
                  method:'POST',
                  headers:{
                    'Content-Type':'application/json',
                    'Content-Length':Buffer.byteLength(data,'utf8')
                  }
                }

                ui_req = http.request options, (ui_res) ->
                  msg.send "Finished running installer on " + robot.instance_name
                  robot.brain.setDefaultStatus()

                ui_req.on 'error', (e) ->
                  robot.messageRoom "30983_hubot@conf.hipchat.com", "Error occurred contacting TestServer"
                  robot.brain.setDefaultStatus()

                ui_req.write data
                ui_req.end

          sql_req.on 'error', (e) ->
            robot.messageRoom "30983_hubot@conf.hipchat.com", "Error occurred contacting SQLServer"
            robot.brain.setDefaultStatus()

          sql_req.write data
          sql_req.end

  robot.respond /(.*) uninstall (core|dmz|all)/i, (msg) ->
    if robot.instance_name == msg.match[1]
      cache = robot.brain.getStatus()

      if cache['locked']
        robot.messageRoom "30983_hubot@conf.hipchat.com", robot.instance_name + " is locked. Try again later"
      else
        robot.messageRoom "30983_hubot@conf.hipchat.com", "Starting uninstaller on " + robot.instance_name
        robot.brain.setStatus({message: "working on Installer", start_time: new Date().getTime(), locked: true})

        arg = msg.match[2]

        exec "python tasks/installer.py uninstall " + arg, (err, stdout, stderr) ->
          robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished uninstalling " + arg + " on " + robot.instance_name
          robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
          msg.send "Done!"
          robot.brain.setDefaultStatus()

