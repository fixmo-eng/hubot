# Description:
#   Starts the automated Installer tests
#
# Dependencies:
#   child_process
#
# Configuration:
#   None
#
# Commands:
#  hubot ([qa|dev]\w*) start (core|dmz) installer - Runs the installer on the specified machine
#

{spawn, exec} = require 'child_process'

module.exports = (robot) ->
  robot.router.post "/hubot/sqlserver/drop", (req, res) ->
    robot.messageRoom "30983_hubot@conf.hipchat.com", "SQLServer received post"
    cache = robot.brain.getStatus()
    if cache['locked']
      res.end robot.instance_name + " is locked.  Closing connection"
    else
      robot.brain.setStatus({message: "Downloading Installers", start_time: new Date().getTime(), locked:true})
      exec "python tasks/sqlserver.py drop_tables", (err, stdout, stderr) ->
        robot.messageRoom "30983_hubot@conf.hipchat.com", "Dropping tables on " + robot.instance_name
        robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
        if stderr
          robot.messageRoom "30983_hubot@conf.hipchat.com", "Error dropping tables on " + robot.instance_name
          robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error dropping tables on " + robot.instance_name
          robot.messageRoom "30983_hubot@conf.hipchat.com", stderr
        res.end "Connection closed"
        robot.brain.setDefaultStatus()
