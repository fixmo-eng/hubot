# Description:
#   Utility commands surrounding Hubot.
#
# Commands:
#   hubot restart - Restart all Hubots
#   hubot update - Update all Hubots (copy over rebuild source)
#   hubot rebuild - Rebuilds Hubot from source, which then other hubots can update from
#   hubot resources - Display Hubot resources
#   hubot rescript - Update python and coffee scripts from ftp

{spawn, exec}  = require 'child_process'

module.exports = (robot) ->
  robot.respond /restart/i, (msg) ->
    msg.send "Restarting [" + robot.instance_name  + "] for an update, be back in a few seconds"
    exec "python tasks/hubot.py restart", (err, stdout, sterr) ->
      msg.send stdout
      msg.send sterr

  robot.respond /rebuild/i, (msg) ->
    if robot.instance_name == "master"
      cache = robot.brain.getStatus()
      if cache['locked']
        msg.send "Already rebuilding source"
      else
        msg.send "Rebuilding [" + robot.instance_name + "] Hubot's source, be back in 2~ minutues"
        robot.brain.setStatus({message: "rebuilding Hubot's source", start_time: new Date().getTime(), locked: true})
        exec "python tasks/hubot.py rebuild", (err, stdout, sterr) ->
          msg.send stdout
          msg.send sterr
          msg.send "Hubot's source has been rebuilt"
          robot.brain.setDefaultStatus()

  robot.respond /update/i, (msg) ->
    msg.send "Updating [" + robot.instance_name + "] Hubot's source, be back in a minute"
    exec "python tasks/hubot.py update " + robot.instance_name, (err, stdout, sterr) ->
      msg.send stdout
      msg.send sterr

  robot.respond /resources/i, (msg) ->
    exec "python tasks/resources.py", (err, stdout, sterr) ->
      msg.send "[" + robot.instance_name + "] Resources:\n" + stdout

  robot.respond /rescript/i, (msg) ->
    exec "python tasks/hubot.py rescript", (err, stdout, sterr) ->
      msg.send stdout
      msg.send stderr
