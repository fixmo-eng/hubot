# Description:
#   Starts the automated UI tests
#
# Dependencies:
#   child_process
#
# Configuration:
#   None
#
# Commands:
#   hubot (name) start UI Tests - Starts the automated UI tests

{spawn, exec}  = require 'child_process'

module.exports = (robot) ->
  robot.router.post "/hubot/master/ui_tests", (req, res) ->
    robot.messageRoom "30983_hubot@conf.hipchat.com", "TestServer received post"
    cache = robot.brain.getStatus()

    if cache['locked']
      robot.messageRoom "30983_hubot@conf.hipchat.com", "Automated UI tests are already running on " + robot.instance_name
      res.end robot.instance_name + " is locked. Try again later"
    else
      res.end "Starting UI Tests"
      robot.messageRoom "30983_hubot@conf.hipchat.com", "Starting automated UI tests on " + robot.instance_name
      robot.brain.setStatus({message: "working on automated UI tests", start_time: new Date().getTime(), locked: true})

      exec "python tasks/ui-tests.py run-all", (err, stdout, stderr) ->
        robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished automated UI tests on " + robot.instance_name
        robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
        if stderr
          robot.messageRoom "30983_hubot@conf.hipchat.com", "Error running ui-tests on " + robot.instance_name
          robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error running ui-tests on " + robot.instance_name
          robot.messageRoom "30983_hubot@conf.hipchat.com", stderr
        robot.brain.setDefaultStatus()

  robot.router.post "/hubot/master/ui_tests_long_timeout", (req, res) ->
    robot.messageRoom "30983_hubot@conf.hipchat.com", "TestServer received post"
    cache = robot.brain.getStatus()

    if cache['locked']
      robot.messageRoom "30983_hubot@conf.hipchat.com", "Automated UI tests are already running on " + robot.instance_name
      res.end robot.instance_name + " is locked. Try again later"
    else
      robot.messageRoom "30983_hubot@conf.hipchat.com", "Starting automated UI tests on " + robot.instance_name
      robot.brain.setStatus({message: "working on automated UI tests", start_time: new Date().getTime(), locked: true})

      exec "python tasks/ui-tests.py run-all", (err, stdout, stderr) ->
        robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished automated UI tests on " + robot.instance_name
        robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
        if stderr
          robot.messageRoom "30983_hubot@conf.hipchat.com", "Error running ui-tests on " + robot.instance_name
          robot.messageRoom "30983_sentinel_dev@conf.hipchat.com", "Error running ui-tests on " + robot.instance_name
          robot.messageRoom "30983_hubot@conf.hipchat.com", stderr
        res.end "Finished running ui-tests"
        robot.brain.setDefaultStatus()

  robot.respond /(\w*) start UI tests/i, (msg) ->
    if robot.instance_name == msg.match[1] && robot.instance_name == "master" && false
      cache = robot.brain.getStatus()

      if cache['locked']
        robot.messageRoom "30983_hubot@conf.hipchat.com", "Automated UI tests are already running on " + robot.instance_name
      else
        robot.messageRoom "30983_hubot@conf.hipchat.com", "Starting automated UI tests on " + robot.instance_name
        robot.brain.setStatus({message: "working on automated UI tests", start_time: new Date().getTime(), locked: true})

        exec "python tasks/ui-tests.py run-all", (err, stdout, stderr) ->
          robot.messageRoom "30983_hubot@conf.hipchat.com", "Finished automated UI tests on " + robot.instance_name
          robot.messageRoom "30983_hubot@conf.hipchat.com", stdout
          if stderr
            robot.messageRoom "30983_hubot@conf.hipchat.com", "Error running ui-tests on " + robot.instance_name
            robot.messageRoom "30983_hubot@conf.hipchat.com", stderr
          robot.brain.setDefaultStatus()
