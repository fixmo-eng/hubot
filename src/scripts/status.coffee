# Description:
#   Hubot(s) will say their name and current status.
#
# Commands:
#   hubot status - hubots response with their name and status

module.exports = (robot) ->
  robot.respond /status/i, (msg) ->
    cache = robot.brain.getStatus()

    # Acquire start time and format it
    time_stamp = format_date(new Date(cache['start_time']))

    msg.send "Since [" + time_stamp + "] [" + robot.instance_name + "] has been " + cache['message']

  format_date = (date) ->
    timeStamp = date.getDate() + "/" + (date.getMonth()+1) + " " + [date.getHours(), date.getMinutes(), date.getSeconds()].join(":")
