"""Script used to manage hubot

This script interacts with hubot and is responsible for installing/starting it.

The environment variable MASTER_HUBOT_ROOT_PATH, HUBOT_ROOT_PATH and HUBOT_NAME
must be set for everything to work.

The environment variables HUBOT_HIPCHAT_JID and HUBOT_HIPCHAT_PASSWORD should
be set if you wish hubot to be used with the Hipchat adaptor.

Usage:
  hubot install
  hubot start [--debug]
  hubot stop
  hubot restart
  hubot rebuild
  hubot update <name>
  hubot config <name>
  hubot -h | --help
  hubot rescript

Options:
  -h --help  Show this screen.
"""

import os
import platform
import time
from utilities import *
import shutil
import subprocess
from docopt import docopt
from config import properties
import paramiko
import ConfigParser

# Required properties
hubot_path = properties.get('hubot.path')
hubot_master_path = properties.get('hubot.master.path')
ftp_parent = properties.get('ftp.parent')
ftp_host = properties.get('ftp.host')
ftp_port = int(properties.get('ftp.port'))
ftp_credentials = properties.get('ftp.credentials')
hubot_ftp_path = properties.get('ftp.hubot.path')

def install():
  os.chdir(hubot_master_path)

  log("Installing Hubot")
  subprocess.call('npm install', shell=True, universal_newlines=True, stdout=open(log_file, 'a'), stderr=open(log_file, 'a'))
  subprocess.call('node node_modules/coffee-script/bin/coffee bin/hubot -c deploy', shell=True, universal_newlines=True, stdout=open(log_file, 'a'), stderr=open(log_file, 'a'))

  # Install deployable hubot
  with cd("deploy"):
    subprocess.call('npm install', shell=True, universal_newlines=True, stdout=open(log_file, 'a'), stderr=open(log_file, 'a'))

  # Copy over scripts
  copy_files('src/*.coffee', 'deploy/node_modules/hubot/')

  # Copy over tasks
  if not os.path.isdir('deploy/tasks/'):
    os.makedirs('deploy/tasks/')
  copy_files('tasks/*', 'deploy/')

def start(debug=False):
  log("Starting Hubot")
  with cd("deploy"):
    if debug:
      subprocess.call('node ../node_modules/coffee-script/bin/coffee  ./node_modules/hubot/bin/hubot -n ' + properties.get('hubot.name'), shell=True, universal_newlines=True)
    else:
      subprocess.Popen('node ../node_modules/coffee-script/bin/coffee  ./node_modules/hubot/bin/hubot -a hipchat -n ' + properties.get('hubot.name'), shell=True, universal_newlines=True, stdout=open(log_file, 'a'), stderr=open(log_file, 'a'))

def stop():
  log("Stoping Hubot")

  # Hacky way to stop hubot
  if platform.system() == "Windows":
    subprocess.call('taskkill /F /IM node.exe', shell=True, universal_newlines=True)
  else:
    subprocess.call('killall node', shell=True, universal_newlines=True)

def restart():
  log("Restarting Hubot")
  stop()
  start()

def update(name='master'):
  log("Updating Hubot")
  stop()

  with cd(hubot_master_path):
    shutil.rmtree(hubot_path)
    copy_master_to_local()

  # Ensure we have the up-to-date name
  config(name)
  start()

def rebuild():
  log("Rebuilding Hubot")

  # Cleaning up master
  with cd(hubot_master_path):
    if os.path.isdir('deploy'):
      shutil.rmtree('deploy')
    if os.path.isdir('node_modules'):
      shutil.rmtree('node_modules')

  install()

def config(name='master'):
  properties.update_name(name)

def copy_master_to_local():
  log("Copying from Master Hubot Path to Hubot Path")
  shutil.copytree(hubot_master_path, hubot_path, ignore=shutil.ignore_patterns('^.git'))

def set_env_vars():
  os.putenv('HUBOT_HIPCHAT_JID', properties.get('hubot.hipchat.jid'))
  os.putenv('HUBOT_HIPCHAT_PASSWORD', properties.get('hubot.hipchat.password'))
  os.putenv('HUBOT_MASTER_PATH', properties.get('hubot.master.path'))
  os.putenv('HUBOT_PATH', properties.get('hubot.path'))

def deploy_scripts():
  shutil.copytree("src/scripts/", "deploy/scripts/", ignore=shutil.ignore_patterns('^.git'))
  shutil.copytree("tasks/", "deploy/tasks/", ignore=shutil.ignore_patterns('^.git'))

def update_scripts():
  client = paramiko.SSHClient()
  client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
  client.load_system_host_keys()

  credentials = ConfigParser.SafeConfigParser()
  credentials.read(ftp_credentials)
  ftp_user = credentials.get('Credentials', 'ftp-username')
  ftp_pass = credentials.get('Credentials', 'ftp-password')

  client.connect(hostname=ftp_host, port=ftp_port, username=ftp_user, password=ftp_pass, timeout=5)
  sftp = client.open_sftp()
  sftp.chdir(hubot_ftp_path)
  sftp.get("src/scripts", hubot_path + "scripts-updated")
  sftp.get("tasks", hubot_path + "tasks-updated")

  with cd("src"):
    shutil.rmtree("scripts", ignore_errors=true)
    os.mkdir("scripts")

  shutil.rmtree("tasks", ignore_errors=true)
  os.mkdir("tasks")

  shutil.move("scripts-updated", "src/scripts")
  shutil.move("tasks-updated", "tasks")
  deploy_scripts()
  restart()

def main():
  arguments = docopt(__doc__)

  # Handle changing name in configuration of property file first
  if arguments['config']:
    config(arguments['<name>'])

  # Set the nessecary environment variables for subprocesses (i.e., Hubot/Hipchat)
  set_env_vars()

  # If we don't have hubot locally then copy it from the master location
  if not os.path.isdir(hubot_path):
    copy_master_to_local()

  # Make sure we are in the hubot's root directory
  os.chdir(hubot_path)

  # Ensure that the log and brain directory exist
  if not os.path.isdir('logs/'):
    os.makedirs('logs/')
  if not os.path.isdir('brains/'):
    os.makedirs('brains/')

  # Parse general arguments
  if arguments['install']:
    install()
  elif arguments['start']:
    start(arguments['--debug'])
  elif arguments['stop']:
    stop()
  elif arguments['restart']:
    restart()
  elif arguments['rebuild']:
    rebuild()
  elif arguments['rescript']:
    update_scripts()
  elif arguments['update']:
    update(arguments['<name>'])

main()
