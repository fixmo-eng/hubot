"""Script used to handle interactions with Installer Test Tool

This script interacts with the system to manage the automated Installer.  It can run the
Installer and verify Installation.

The variable installer.path must be specified in the config.properties file.

Usage:
  installer setup
  installer setup_files [--build=<build>]
  installer run (core|dmz|all)
  installer test (core|dmz|all)
  installer uninstall (core|dmz|all)
  installer -h | --help

Options:
  -h --help  Show this screen.
"""

import os
import re
import shutil
import urllib2
import ConfigParser
import paramiko
from utilities import *
from docopt import docopt
import subprocess
from config import properties

CORE_INSTALL_CONFIG="FixmoInstallerConfig.txt"
DMZ_INSTALL_CONFIG="FixmoDMZInstallerConfig.txt"
installer_path = properties.get('installer.path')
relay_bundle_path = properties.get('relay.bundle.path')
ftp_parent = properties.get('ftp.parent')
ftp_host = properties.get('ftp.host')
ftp_port = int(properties.get('ftp.port'))
ftp_credentials = properties.get('ftp.credentials')

def setup():
  log("Setting up Installer")
  subprocess.call('python setup.py build install', shell=True, universal_newlines=True)

def setup_files(url="latest"):
  # Identify the files to download
  client = paramiko.SSHClient()
  client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
  client.load_system_host_keys()
  
  credentials = ConfigParser.SafeConfigParser()
  credentials.read(ftp_credentials)
  ftp_user = credentials.get('Credentials', 'ftp-username')
  ftp_pass = credentials.get('Credentials', 'ftp-password')
  
  client.connect(hostname=ftp_host, port=ftp_port, username=ftp_user, password=ftp_pass, timeout=5)
  sftp = client.open_sftp()
  sftp.chdir(ftp_parent + url)
  for f in sftp.listdir("."):
    match = re.search("Fixmo EMP (Core|DMZ)-(\d\.\d\.\d+)\.exe", f)
    if match is not None:
      sftp.get(f, installer_path + f)
      installer_type = match.group(1)
      conf = None
      if installer_type == "Core":
        conf = CORE_INSTALL_CONFIG
      elif installer_type == "DMZ":
        conf = DMZ_INSTALL_CONFIG

      if conf != None:
        parser = ConfigParser.SafeConfigParser()
        parser.read(installer_path + conf)
        parser.set('Installer Options', 'installer-location', match.group(0))
        if installer_type == "DMZ":
          parser.set('POT File', 'pot-file', relay_bundle_path)
        parser.write(open(installer_path + conf, 'w+'))

def install(arg):
  log("Installing " + arg)
  subprocess.call('python InstallerTestTool.py ' + arg, shell=True, universal_newlines=True)

def test_install(arg):
  log("Testing Installation of " + arg)
  subprocess.call('python InstallerTestTool.py -si ' + arg, shell=True, universal_newlines=True)

def uninstall(arg, conf):
  log("Uninstalling Installation of " + arg)
  subprocess.call('python InstallerTestTool.py --uninstall-' + arg + ' ' + conf, shell=True, universal_newlines=True)

def change_dir_to_installer():
  # Navigate to installer directory
  os.chdir(installer_path)

def main():
  arguments = docopt(__doc__)

  if arguments['setup']:
    change_dir_to_installer()
    setup()

  elif arguments['setup_files']:
    if arguments['--build']:
      setup_files(arguments['--build'])
    else:
      setup_files()

  elif arguments['run']:
    change_dir_to_installer()
    if arguments['core']:
      install(CORE_INSTALL_CONFIG)
    elif arguments['dmz']:
      install("-D " + DMZ_INSTALL_CONFIG)
    elif arguments['all']:
      install(CORE_INSTALL_CONFIG)
      install("-D " + DMZ_INSTALL_CONFIG)

  elif arguments['test']:
    change_dir_to_installer()
    if arguments['core']:
      test_install(CORE_INSTALL_CONFIG)
    elif arguments['dmz']:
      test_install("-D " + DMZ_INSTALL_CONFIG)
    elif arguments['all']:
      test_install(CORE_INSTALL_CONFIG)
      test_install("-D " + DMZ_INSTALL_CONFIG)

  elif arguments['uninstall']:
    change_dir_to_installer()
    if arguments['core']:
      uninstall('core', CORE_INSTALL_CONFIG);
    elif arguments['dmz']:
      uninstall('dmz', DMZ_INSTALL_CONFIG)
    elif arguments['all']:
      uninstall('core', CORE_INSTALL_CONFIG)
      uninstall('dmz', DMZ_INSTALL_CONFIG)

main()
