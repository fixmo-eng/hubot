"""Script used to handle interactions with Automated UI Tests

This script interacts with the system to manage the automated UI tests. The
must be configured to support running these automated UI tests.

The environment variable UI_TEST_PATH must be set, which indicates the root
directory of the UI tests.

Usage:
  ui-tests run-all
  ui-tests run <test>
  ui-tests -h | --help

Options:
  -h --help  Show this screen.
"""

import os
from utilities import *
from docopt import docopt
import subprocess
from config import properties

def run_all_tests():
  log("Running all tests")
  subprocess.call('ant test', shell=True, universal_newlines=True)


def run_test(test):
  log("Running test " + test)
  subprocess.call('ant run-test -Dtest=' + test, shell=True, universal_newlines=True)

def change_dir_to_ui_tests():
  # Navigate to UI tests directory
  ui_test_path = properties.get('ui.tests.path')
  os.chdir(ui_test_path)

def main():
  arguments = docopt(__doc__)
  change_dir_to_ui_tests()

  if arguments['run-all']:
    run_all_tests()

  elif arguments['run']:
    test = arguments['<test>']
    run_test(test)
  os.chdir(properties.get('hubot.path'))

main()
