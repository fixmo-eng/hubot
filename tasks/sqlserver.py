"""Script used to handle interactions with the SQLServer database

This script should only ever be executed on SQLServer at the end of an automated installer run

Usage:
  sqlserver drop_tables

Options:
  -h --help  Show this screen.
"""

import os
import re
import ConfigParser
from utilities import *
from docopt import docopt
import subprocess
from config import properties

def drop_tables():
  sql_user = properties.get('sql.user')
  sql_password = properties.get('sql.password')
  sql_host = 'localhost'
  sql_db = properties.get('sql.db')
  proc = subprocess.Popen(['sqlcmd', '-U', sql_user, '-P', sql_password, '-d', sql_db, '-Q',
      properties.get('hubot.path') + 'tasks\\' + 'drop_everything'], stdout = subprocess.PIPE)

def main():
  arguments = docopt(__doc__)

  if arguments['drop_tables']:
    drop_tables()

main()
