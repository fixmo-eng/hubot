import os
import StringIO
import ConfigParser

class parser:
  def __init__(self, config_file):
    self.config_file = config_file

    self.parser = ConfigParser.SafeConfigParser()
    self.parser.read(self.config_file)

  def get(self, property_key):
    # Acquire name of this hubot
    name = str(self.parser.get('hubot', 'hubot.name'))

    # Try to acquire value of property from named hubot's config
    try:
      value = str(self.parser.get(name, property_key))
    except ConfigParser.NoOptionError as e:
      # If named hubot's config doesn't have value, try general
      value = str(self.parser.get('hubot', property_key))

    return value

  def update_name(self, name):
    self.parser.set('hubot', 'hubot.name', name)
    self.parser.write(open(self.config_file, 'w+'))
    self.parser.read(self.config_file)

# Properties we can access from the config.properties file
properties = parser(os.path.dirname(os.path.realpath(__file__)) + '/config.properties')

