import os
import glob
import shutil
import datetime
from config import properties

log_file = properties.get('hubot.path') + '/logs/' + properties.get('hubot.name') + '.log'

class cd:
  """Context manager for changing the current working directory, and providing useful functions"""
  # http://stackoverflow.com/a/13197763/583592
  def __init__(self, newPath):
    self.newPath = newPath

  def __enter__(self):
    self.savedPath = os.getcwd()
    os.chdir(self.newPath)

  def __exit__(self, etype, value, traceback):
    os.chdir(self.savedPath)

# http://stackoverflow.com/a/2584474/583592
def copy_files(src_glob, dst_folder):
  for fname in glob.iglob(src_glob):
    shutil.copy(fname, os.path.join(dst_folder, fname))

def log(message):
  with open(log_file + '.tasks', 'a') as f:
    f.write("[" + str(datetime.datetime.now()) + "] " + str(message) + "\n")
