import platform
import subprocess

if platform.system() == "Windows":
  cpu = subprocess.Popen('wmic cpu get loadpercentage', shell=True, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  mem = subprocess.Popen('systeminfo |find "Available Physical Memory"', shell=True, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

  cpu_out, cpu_err = cpu.communicate()
  mem_out, mem_err = mem.communicate()

  print mem_out + "\n" + cpu_out

else:
  print "Unsupport OS"
